﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatSys : MonoBehaviour
{
    public Transform atkPoint;
    public float attackRange;
    public int damage;
    public float attackRate;
    private float nextAtkTime;
    public LayerMask enemyLayer;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextAtkTime)
        {
            
            if (Input.GetKeyDown(KeyCode.X))
            {
                nextAtkTime = Time.time + 1f /  attackRate;
                AttackFront();
            }
        }

    }

    void AttackFront()
    {

        Collider2D[] enemies = Physics2D.OverlapCircleAll(atkPoint.position, attackRange, enemyLayer);

        for(int i = 0; i < enemies.Length; i++)
        {
            Debug.Log("A");
            enemies[i].GetComponent<EnemyHpSys>().takeDamage(damage);
        }
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(atkPoint.position, attackRange);
    }
}
