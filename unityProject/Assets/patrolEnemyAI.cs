﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class patrolEnemyAI : MonoBehaviour
{
    public float speed;
    public float changePointDist;
    public GameObject[] waypoints;
    Rigidbody2D rb;
    int nextWaypoint = 0;

    float distToPoint;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Move();
    }

    void Move()
    {
        distToPoint = Vector2.Distance(transform.position, waypoints[nextWaypoint].transform.position);

        Vector3 wayPointTransform = waypoints[nextWaypoint].transform.position;
        wayPointTransform.y = transform.position.y;
        transform.position = Vector2.MoveTowards(transform.position, wayPointTransform , speed * Time.deltaTime);


        if (distToPoint < changePointDist)
        {
            Vector3 scaler = transform.localScale;
            scaler.x *= -1;
            transform.localScale = scaler;
            nextWaypoint = nextWaypoint + 1 == waypoints.Length ? 0 : nextWaypoint + 1;
        }
    }
}
