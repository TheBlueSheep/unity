using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpManager : MonoBehaviour
{

    public Text text;
    public int startHp;
    private int currHp;
    void Start()
    {
        currHp = startHp;
        updateHp();
    }
    
    public void updateHp()
    {
        text.text = currHp.ToString("0");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("A");
        if(collision.CompareTag("Enemy"))
        {
            currHp -= 1;
            updateHp();
        }
    }
    
}



