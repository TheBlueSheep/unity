using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHpSys : MonoBehaviour
{

    public int hpAmount;
    private int currHp;
    // Start is called before the first frame update
    void Start()
    {
        currHp = hpAmount;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void takeDamage(int dmg)
    {
        currHp -= dmg;

        if (currHp <= 0)
        {
            die();
        }
    }

    void die()
    {
        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;
        Destroy(gameObject);
    }
}
