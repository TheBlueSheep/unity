﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    private float moveInput;

    public Animator animator;
    private Rigidbody2D rb;

    private float gravity;
    //check for ground and stuff
    private bool facingRight = true;
    private bool onGround;
    private float checkRadiusGround;
    public LayerMask whatIsGround;
    public Transform groundChecker;

    //jump vars
    private float jumpTimeCounter;
    public float jumpHeight;
    public float jumpTime;
    private bool isJumping;

    public LayerMask whatIsWall;

    //wall jump variables
    private bool canWallJump;
    public float wallSlideSpeed;
    public Transform frontCheck;
    private bool isTouchingFront;
    bool wallSliding;
    private float checkRadiusWall;
    public float xWallForce;
    public float yWallForce;
    public float wallJumpTime;
    bool wallJumping;
    float wallJumpTimeCounter;

    public float maxFallSpeed;

    //dash vars
    public float dashTime;
    public float dashSpeed;
    private bool canDash;
    private bool isDashing;
    private float timeLeftDashing;
    public float dashCoolDown;
    private float dashCoolDownLeft;
    private bool canDashNow;


    public ParticleSystem dashParticles;


    //double jump
    private bool canDoubleJump;
    private bool didDoubleJump;




    void Start()
    {
        animator = GetComponent<Animator>();
        dashParticles = GetComponent<ParticleSystem>();
        dashParticles.Stop();
        rb = GetComponent<Rigidbody2D>();
        gravity = rb.gravityScale;
        checkRadiusGround = 0.2F;
        checkRadiusWall = 0.3F;
        dashCoolDown = 1;
        
        canWallJump = true;
        canDash = true;
        canDoubleJump = false;
    }

    private void FixedUpdate()
    {
        if (!wallJumping && !isDashing)
        {
            moveInput = Input.GetAxisRaw("Horizontal");
            rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

            if (facingRight && moveInput < 0)
            {
                Flip();
            }
            if (!facingRight && moveInput > 0)
            {
                Flip();
            }
        }


        onGround = Physics2D.OverlapCircle(groundChecker.position, checkRadiusGround, whatIsGround);
        isTouchingFront = Physics2D.OverlapCircle(frontCheck.position, checkRadiusWall, whatIsWall);
    }

    // Update is called once per frame
    void Update()
    {
        if(moveInput != 0 && onGround)
        {
            animator.SetBool("Running", true);
        }
        else
        {
            animator.SetBool("Running", false);
        }
        if(onGround)
        {
            animator.SetBool("Jumping", false);
        }


        if(onGround || wallSliding)
        {
            didDoubleJump = false;
        }
        //allows double jump
       /* if (!onGround && !isTouchingFront && !didDoubleJump)
        {
            canDoubleJump = true;
        }
        else
        {
            canDoubleJump = false;
        } */ 

        //initiate jump
        if ((Input.GetKeyDown(KeyCode.Space) && (onGround || canDoubleJump)))
        {
            jump();
            animator.SetBool("Jumping", true);
        }

        //continuing Jump
        if (Input.GetKey(KeyCode.Space) && isJumping)
        {
            continueJump();
        }
        else if(isJumping)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            isJumping = false;
        }


        checkMaxFallSpeed();


        if (canWallJump)
        {
            if (isTouchingFront && !onGround && moveInput != 0)
            {
                wallSliding = true;
                animator.SetBool("WallSlide", true);
            }
            else if (!(wallSliding && isTouchingFront && !onGround))
            {
                wallSliding = false;
                animator.SetBool("WallSlide", false);
            }

            if (wallSliding)
            {
                //when walll sliding the velocity is constant
                rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -wallSlideSpeed, float.MaxValue));
            }

            if (Input.GetKeyDown(KeyCode.Space) && wallSliding)
            {
                wallJump();
            }

            if (wallJumpTimeCounter < 0)
            {
                wallJumping = false;
            }

            if (wallJumping)
            {
                //wall jump is a alway in the opposite dir of the wall for a certain amount of time
                continueWallJump();
            }
        }


        dashCoolDownLeft -= Time.deltaTime;
        canDashNow = checkCanDash() || canDashNow;

        //dash code
        //when dashing i change gravity to 0 so it will be horizontal (the player wont fall)
        if (canDash)
        {
            if (Input.GetKeyDown(KeyCode.C) && canDashNow)
            {
                dash();
            }

            if (isDashing)
            {
                rb.velocity = new Vector2(facingRight ? dashSpeed : -dashSpeed, 0);
                timeLeftDashing -= Time.deltaTime;
            }

            if (isDashing && timeLeftDashing <= 0)
            {
                dashCoolDownLeft = dashCoolDown;
                isDashing = false;
                rb.gravityScale = gravity;
            }

        }

    }


    //this function doesnt let the player fall faster than max fall speed
    void checkMaxFallSpeed()
    {
        if (rb.velocity.y < -maxFallSpeed)
        {
            rb.velocity = new Vector2(rb.velocity.x, -maxFallSpeed);
        }

    }

    //checks if the player can dash and returns the output
    bool checkCanDash()
    { 

        if ((onGround || wallSliding) && (dashCoolDownLeft < 0))
        {
            return true;
        }
        return false;

    }

    void dash()
    {
        if (!dashParticles.isPlaying)
        {
            dashParticles.Play();
        }
        isDashing = true;
        if(wallSliding)
        {
            Flip();
        }
        timeLeftDashing = dashTime;
        rb.gravityScale = 0;

    }
    void wallJump()
    {

        wallJumping = true;
        wallSliding = false;
        wallJumpTimeCounter = wallJumpTime;
        Flip();
    }

    void continueWallJump()
    {
        wallJumpTimeCounter -= Time.deltaTime;
        if (moveInput == 0)
        {
            rb.velocity = new Vector2(xWallForce * -(facingRight ? -1 : 1), yWallForce);
        }
        else
        {
            rb.velocity = new Vector2(xWallForce * -moveInput, yWallForce);
        }
    }

    void jump()
    {

        if (canDoubleJump)
        {
            didDoubleJump = true;
            canDoubleJump = false;
        }

        isJumping = true;
        jumpTimeCounter = jumpTime;
        rb.velocity = Vector2.up * (float)(jumpHeight);
        jumpTimeCounter -= Time.deltaTime;

    }


    void continueJump()
    {

        if (jumpTimeCounter > 0)
        {
            rb.velocity = Vector2.up * (float)(jumpHeight);
            jumpTimeCounter -= Time.deltaTime;
        }
        else
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            isJumping = false;
        }
    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }
}
